<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 06/06/2018
 * Time: 22:13
 */

namespace App\Baker;

/**
 * Class MessageBaker
 * @package App\Baker
 */
class MessageBaker extends Baker
{
    /**
     * @return int
     */
    public function byte(): int
    {
        return SODIUM_CRYPTO_SECRETBOX_NONCEBYTES;
    }

    /**
     * @return string
     */
    public function key(): string
    {
        return sodium_crypto_secretbox_keygen();
    }
}