<?php
/**
 * Created by PhpStorm.
 * User: andrewsokolic
 * Date: 06/06/2018
 * Time: 21:16
 */

namespace App\Baker;

/**
 * Class Baker
 *
 * this is a simple abstract class to make handling the sodium lib
 *
 * @package App\Baker
 */
abstract class Baker
{
    /** @var string */
    private $sodiumKey;

    /** @var string */
    private $sodiumByte = SODIUM_CRYPTO_SECRETBOX_NONCEBYTES;

    /**
     * Get the secret key from the child
     *
     * @return string
     */
    abstract protected function key() : string;

    /**
     * Get the bytes from the child
     *
     * @return int
     */
    abstract protected function byte() : int;

    /**
     * Baker constructor.
     */
    public function __construct()
    {
        $this->sodiumKey = $this->key();
        $this->sodiumByte = $this->byte();
    }

    /**
     * Encrypt using the core sodium lib
     *
     * @param string recipe
     * @return string
     * @throws \Exception
     */
    final public function bake($recipe){
        $dough = random_bytes($this->sodiumByte);

        $cake = base64_encode(
            $dough.
            sodium_crypto_secretbox(
                $recipe,
                $dough,
                $this->sodiumKey
            )
        );
        sodium_memzero($recipe, strlen($recipe));
        sodium_memzero($this->sodiumKey, strlen($this->sodiumKey));
        return $cake;
    }

    /**
     * Decrypt using the core sodium lib
     *
     * @param string $cake
     * @return bool|string
     * @throws \Exception
     */
    final public function unbake($cake){
        $decoded = base64_decode($cake);
        if ($decoded === false) {
            throw new \Exception('Unable to unbake your cake!');
        }
        if (mb_strlen($decoded, '8bit') < ($this->sodiumByte + SODIUM_CRYPTO_SECRETBOX_MACBYTES)) {
            throw new \Exception('it seems your cake is missing!');
        }
        $dough = mb_substr($decoded, 0, $this->sodiumByte, '8bit');
        $cake = mb_substr($decoded, $this->sodiumByte, null, '8bit');

        $recipe = sodium_crypto_secretbox_open(
            $cake,
            $dough,
            $this->sodiumKey
        );
        if ($recipe === false) {
            throw new \Exception('Your cake has been touched!');
        }
        sodium_memzero($cake, strlen($cake));
        sodium_memzero($this->sodiumKey, strlen($this->sodiumKey));
        return $recipe;
    }

}